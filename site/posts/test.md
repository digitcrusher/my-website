---
title: Initial Commit
date: 2022-02-06
---
This is just a page I use to test the CSS on this website. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet tempus purus. In dictum est `hendrerit malesuada scelerisque`. Phasellus leo felis, porta vitae lobortis ac, gravida et sapien. [Mauris fringilla](/) ligula sed neque eleifend, quis efficitur leo tempor.

- Lorem ipsum
- dolor sit amet,
- consectetur adipiscing elit.

"Nullam tincidunt ipsum non dolor fermentum congue. Duis euismod rutrum libero, eget lobortis ex sodales in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis orci consequat libero tincidunt viverra vitae vel mi." Praesent non iaculis ante, ut molestie ante.

### 🤔️ Python's .replace('abc', '123')

```python
from typing import Iterator

# This is an example
class Math:
    @staticmethod
    def fib(n: int) -> Iterator[int]:
        """ Fibonacci series up to n """
        a, b = 0, 1
        while a < n:
            yield a
            a, b = b, a + b

result = sum(Math.fib(42))
print("The answer is {}".format(result))
```

> > Sed vel posuere velit, id sodales diam.
>
>     :::cpp
>     Something?
> Vestibulum et purus sed magna auctor lacinia a non nulla. Sed id quam ipsum. Mauris eros erat, fringilla sit amet lectus ut, semper posuere purus. Quisque ac nulla elit. Donec massa dolor, placerat id ornare sed, sodales nec augue. Sed vel nibh eget ligula condimentum suscipit.

Number|Squared|Cubed|Fourth power
------|:------|:---:|-----------:
1     |1      |  1  |           1
2     |4      |  8  |          16
3     |9      | 27  |          81

### Nixie Tubes
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Text+Me+One&display=swap">
<style>
.nixie-tubes {
  text-shadow: 0 0 0.1em red, 0 0 0.1em red, 0 0 0.5em red, 0 0 1em red;
  font-size: 200%;
  font-family: 'Text Me One', sans-serif;
  color: #ffff40;
}
</style>
<span class="nixie-tubes">1234567890.-</span>

---

## Lorem Ipsum
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed ante ac leo placerat pretium. Etiam sollicitudin dui lectus. Sed tincidunt justo nec tellus sodales condimentum. Suspendisse auctor egestas lacus, non faucibus elit facilisis eu. Fusce scelerisque at lacus a tincidunt. Nullam eu tempor enim. Vestibulum pretium nulla pretium, imperdiet nibh vestibulum, porta mauris. Vestibulum commodo nunc sed sem sagittis, a posuere purus dictum. In porttitor lorem in neque porttitor, ac aliquet purus consequat. Maecenas porttitor scelerisque luctus. Nunc dui ipsum, pulvinar vitae ante a, bibendum cursus nibh. Duis dictum, dolor ut sodales scelerisque, elit velit eleifend neque, tincidunt lacinia ligula sapien id mauris. Etiam urna quam, sodales vel risus ac, vestibulum sagittis sapien.

Nullam laoreet massa eu erat sagittis, et convallis diam auctor. Pellentesque scelerisque cursus felis interdum aliquam. Nunc laoreet, ligula id interdum finibus, risus eros euismod sapien, id faucibus mi lectus id felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean cursus mi nec aliquam tempus. Donec laoreet, odio vel gravida fringilla, lacus nisl posuere justo, sit amet sodales purus ligula vitae lectus. Morbi sodales orci eget mi blandit, vitae lobortis justo ultrices. Sed sit amet ex nisi. Cras tempus facilisis sodales.

In iaculis sem quis turpis ultricies, sed eleifend sapien posuere. In tincidunt mattis sem, ac imperdiet magna porttitor eget. Praesent dui urna, rutrum ornare elit vitae, vulputate tristique ligula. Nullam eleifend sem at lectus molestie porttitor. Nam in libero condimentum, imperdiet massa pulvinar, tincidunt lorem. Ut eget purus leo. Praesent cursus auctor diam vel sodales. Integer mattis lorem velit, eu euismod mauris accumsan sit amet. In molestie arcu sapien, ut accumsan elit finibus eu. Aliquam malesuada venenatis erat, ut elementum enim ornare vel. Phasellus vitae turpis fringilla, dapibus lorem eget, dignissim orci. Curabitur tortor est, cursus eget odio sit amet, hendrerit ultrices metus. Duis ut dolor ac dui commodo pellentesque non finibus odio. Ut eros leo, imperdiet et tempor quis, maximus vestibulum lectus. Duis volutpat pharetra arcu malesuada sagittis.

Suspendisse ultrices lacus velit, nec efficitur tellus venenatis semper. Etiam ut ante nisl. Nullam eget sodales mauris, ut pellentesque augue. Praesent hendrerit diam semper pretium posuere. Ut quam risus, lobortis vitae placerat et, dignissim ac enim. Ut et turpis sapien. Nullam condimentum, velit a rutrum dignissim, sem est sagittis felis, id fermentum metus leo ut nibh. Nullam facilisis malesuada diam quis facilisis. Maecenas tempor dolor id elit condimentum mattis. Maecenas elit ligula, luctus a auctor nec, sodales at sem. Pellentesque consequat vitae odio quis maximus. Sed at quam arcu. Integer et nisl sed sem hendrerit pharetra.

Aliquam tempus turpis non commodo volutpat. Morbi congue, tellus eu pretium convallis, dui nunc sagittis urna, dapibus posuere nulla sem id magna. Quisque dapibus nunc sed lacinia egestas. Sed sit amet urna ut velit eleifend tempus eget ac lectus. Cras mauris velit, facilisis nec arcu sed, gravida ultricies lectus. Curabitur et bibendum ante. Aenean fringilla sollicitudin erat, eu tincidunt turpis fermentum vitae. Etiam volutpat ut purus et sodales. Etiam magna nisl, malesuada a urna ac, consequat venenatis ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam mattis, sapien nec sollicitudin blandit, sapien libero rutrum mauris, eu blandit massa diam sed velit.

Nulla sit amet vestibulum metus, vitae varius eros. Mauris ullamcorper, nunc id auctor ultrices, purus purus luctus est, a ultricies purus quam non purus. Duis ultrices lobortis tristique. Aliquam et sapien felis. In faucibus, arcu sed luctus euismod, turpis arcu laoreet sem, nec porttitor diam ligula non massa. Morbi fringilla at magna ullamcorper aliquet. Aliquam placerat condimentum magna. Maecenas rhoncus dui nec libero tincidunt bibendum id ac ex. Aliquam eu iaculis tellus, ut viverra felis. Phasellus sit amet quam malesuada, volutpat dui ac, viverra felis. Vivamus vel dolor felis. Cras nec nulla in nisi tempus faucibus non at risus.

Nunc ultrices sodales justo. Mauris eget interdum mauris. Vivamus eget fermentum metus. Maecenas vitae dignissim purus, quis tincidunt orci. Fusce bibendum diam vel elementum ultrices. Duis sit amet finibus purus. Aenean id volutpat diam, in aliquet metus. Cras congue, felis id pellentesque dapibus, neque justo imperdiet nulla, nec pulvinar diam sem et arcu. Duis aliquam ornare volutpat. In tempus pharetra turpis, non dictum orci pellentesque sit amet. Aenean quis magna finibus, tincidunt nibh rhoncus, commodo sapien. Quisque eget accumsan quam.

Etiam sed bibendum purus, ut faucibus massa. Nulla faucibus sapien at dui luctus efficitur. Phasellus faucibus dolor ac elit iaculis dapibus. Nullam viverra accumsan velit aliquet pharetra. Donec consectetur vestibulum diam, ac euismod tellus pellentesque nec. In tellus nisl, tincidunt id quam condimentum, gravida hendrerit sem. Aenean sagittis non turpis interdum congue. Aenean iaculis tincidunt ex, sed blandit leo vestibulum vel. Ut eu egestas lorem, vitae suscipit quam.

Etiam eget rutrum ex, at dictum arcu. Praesent scelerisque imperdiet risus id bibendum. Pellentesque pulvinar malesuada erat, quis aliquet nulla gravida gravida. Proin fermentum purus ac placerat consequat. Curabitur scelerisque rutrum viverra. Duis at vehicula sapien, at lacinia dolor. Praesent luctus molestie ex, sit amet volutpat turpis varius in. Ut in iaculis enim, eleifend tristique neque. Curabitur venenatis pretium lorem at dignissim.

Quisque vulputate ultricies tortor. Integer auctor libero eu augue iaculis, id elementum sem posuere. Ut laoreet, tellus eu ultrices lacinia, diam diam malesuada ante, et pretium felis odio vel augue. Donec in accumsan mi. Phasellus id bibendum magna. Maecenas felis elit, tristique ac finibus ac, hendrerit ut purus. Donec libero mauris, malesuada quis placerat id, malesuada nec erat. Nulla volutpat lacinia gravida. Donec tristique nulla volutpat tempus venenatis. Nam tristique dui in magna gravida congue. Aliquam erat volutpat. Ut ut sollicitudin nunc. Fusce viverra nibh sit amet massa aliquet ultrices nec sed massa. Quisque elit erat, hendrerit quis dapibus nec, ullamcorper eu mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
