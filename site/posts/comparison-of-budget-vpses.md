---
title: Comparison of Budget VPSes
date: 2022-02-20
---
The following specs are taken from the cheapest offering of a given hosting provider. All of these VPSes are based on KVM and the prices include VAT in Poland (23%). You may also want to check out [VPSBenchmarks](https://www.vpsbenchmarks.com/) for more in-depth rankings of VPSes based in anglophone countries.

**{{ '2022-03-08' | human_date }}:** I've chosen [IQhost][iqhost] and it works fine for my usecase (running SSH remote port forwarding for this website and a relay mail server).

Name                      |Yearly cost| RAM |vCPU|Storage|Bandwidth|  SLA  |          CPU          |Notes
--------------------------|:---------:|:---:|:--:|:-----:|:-------:|:-----:|:---------------------:|-----
[IQhost][iqhost]          |   89.70zł | 1GB |  1 |  10GB | 250Mb/s |   ?   |         2.2Ghz        |DDoS protection, reverse DNS, 1TB transfer limit
[HosTeam][hosteam]        |  121.77zł | 1GB |  1 |  15GB | 100Mb/s |   ?   |           ?           |
[Sprint][sprint]          |  141.70zł | 1GB |  1 |  10GB |    ?    | 99.90%|           ?           |DDoS protection, 5TB/month transfer limit
[forpsi][forpsi]          |  176.97zł | 1GB |  1 |  20GB |    ?    |   ?   |           ?           |VMware, 2TB/month transfer limit
[Cal.pl][cal.pl]          |  183.27zł | 1GiB|  1 |  20GB |    ?    | 99.90%|           ?           |
[Aruba Cloud][aruba]      |   41.18€  | 1GB |  1 |  20GB |    ?    | 99.80%|Xeon E5-2650L v4 1.7Ghz|DDoS protection, reverse DNS, VMware, 2TB/month transfer limit
[OVHcloud/Kimsufi][ovh]   |  191.88zł | 2GB |  1 |  20GB | 100Mb/s | 99.95%|           ?           |DDoS protection, reverse DNS, expandable storage
[Vultr][vultr]            |   51.66$  |0.5GB|  1 |  10GB |    ?    |100.00%|           ?           |0.5TB/month transfer limit
[Servizza][servizza]      |  221.40zł | 2GB |  1 |  20GB |   1Gb/s | 99.90%|  Xeon Skylake 2.1GHz  |Reverse DNS, expandable storage, 20TB/month transfer limit, located in Germany
[BlazingFast][blazingfast]|   59.04€  | 1GB |  1 |  30GB |    ?    |   ?   |    Epyc 7702P 2GHz    |DDoS protection, expandable storage
[Hetzner][hetzner]        |   61.25€  | 2GB |  1 |  20GB |    ?    |   ?   |           ?           |DDoS protection, reverse DNS, expandable storage, 20TB/month transfer limit
[Nazwa.pl][nazwa.pl]      |  332.10zł |0.5GB|  1 |  10GB |    ?    |   ?   |    Xeon E5 2.2 GHz    |DDoS protection, reverse DNS
[SLDC][sldc]              |  348.00zł | 2GB |  1 |  50GB | 100Mb/s | 99.99%|           ?           |Poor customer support
[Linux.pl][linux.pl]      |  412.05zł | 2GB |  1 |  20GB | 100Mb/s | 99.90%|           ?           |20TB/month transfer limit
[Webtropia][webtropia]    |   99.49€  | 1GB |  1 |  30GB | 100Mb/s | 99.00%|           ?           |Reverse DNS
[Scaleway][scaleway]      |  106.27€  | 2GB |  2 |  20GB | 200Mb/s |   ?   |       Epyc 7281       |DDoS protection, reverse DNS
[FireHost][firehost]      |  516.60zł | 1GiB|  ? |  25GB |  50Mb/s |   ?   |    i7-980x 3.33GHz    |500GB/month transfer limit
[HostingsHouse][hosthouse]|  132.11€  | 1GB |  1 |  25GB |    ?    |   ?   |           ?           |1TB/month transfer limit

[sldc]: https://panel.sldc.eu/index.php?rp=/store/vps-kvm
[nazwa.pl]: https://www.nazwa.pl/vps/
[ovh]: https://www.ovhcloud.com/pl/vps/
[hosteam]: https://hosteam.pl/pl/vps
[aruba]: https://www.arubacloud.com/vps/virtual-private-server-range.aspx
[blazingfast]: https://blazingfast.io/vps
[scaleway]: https://www.scaleway.com/en/virtual-instances/development/
[iqhost]: https://www.iqhost.eu/serwery-cloud-fast/
[cal.pl]: https://www.cal.pl/vps
[firehost]: https://firehost.pl/cart.php?gid=3
[forpsi]: https://www.forpsi.pl/virtual/
[sprint]: https://www.sprintdatacenter.pl/serwer-wirtualny-tani-vps/
[servizza]: https://servizza.com/serwery-vps-cloud
[hosthouse]: https://www.hostingshouse.com/cart.php?gid=43
[hetzner]: https://www.hetzner.com/cloud
[linux.pl]: https://hosting.linux.pl/serwery-vps.html
[webtropia]: https://www.webtropia.com/en/vps-hosting/linux.html
[vultr]: https://www.vultr.com/products/cloud-compute/
