/*
 * A vote simulation program intended for a blog post
 *
 * Copyright (c) 2022 Karol Łacina aka digitcrusher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <algorithm>
#include <array>
#include <cassert>
#include <climits>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>
#include <queue>
#include <random>
#include <vector>

using namespace std;

int const opinion_range = 100;
bool const is_satisfaction_relative = false;
bool const are_winners_ordered = true;

default_random_engine rng;

double satisfaction_linear(vector<vector<int>> const& opinions, vector<int> const& winners) {
  int const voterc = opinions.size(), winnerc = winners.size();

  double result = 0;
  for(auto &voter: opinions) {
    int favorite = INT_MIN;
    if(is_satisfaction_relative) {
      for(int choice: voter) {
        favorite = max(favorite, choice);
      }
    }
    for(int i = 0; i < winnerc; i++) {
      int x = voter[winners[i]];
      if(is_satisfaction_relative) {
        x = x - favorite + opinion_range;
      }
      if(are_winners_ordered) {
        x = x * (winnerc - i);
      }
      result += x;
    }
  }
  if(are_winners_ordered) {
    result /= winnerc * (winnerc + 1) / 2;
  } else {
    result /= winnerc;
  }
  result /= voterc;
  return result;
}

double satisfaction_square(vector<vector<int>> const& opinions, vector<int> const& winners) {
  int const voterc = opinions.size(), winnerc = winners.size();

  double result = 0;
  for(auto &voter: opinions) {
    int favorite = INT_MIN;
    if(is_satisfaction_relative) {
      for(int choice: voter) {
        favorite = max(favorite, choice);
      }
    }
    for(int i = 0; i < winnerc; i++) {
      int x = voter[winners[i]];
      if(is_satisfaction_relative) {
        x = x - favorite + opinion_range;
      }
      x = x * x * (x >= 0 ? 1 : -1);
      if(are_winners_ordered) {
        x = x * (winnerc - i);
      }
      result += x;
    }
  }
  result /= opinion_range;
  if(are_winners_ordered) {
    result /= winnerc * (winnerc + 1) / 2;
  } else {
    result /= winnerc;
  }
  result /= voterc;
  return result;
}

double satisfaction_discrete(vector<vector<int>> const& opinions, vector<int> const& winners) {
  int const voterc = opinions.size(), winnerc = winners.size();

  double result = 0;
  for(auto &voter: opinions) {
    for(int i = 0; i < winnerc; i++) {
      int x = max(-1, min(1, voter[winners[i]]));
      if(are_winners_ordered) {
        x = x * (winnerc - i);
      }
      result += x;
    }
  }
  result *= opinion_range;
  if(are_winners_ordered) {
    result /= winnerc * (winnerc + 1) / 2;
  } else {
    result /= winnerc;
  }
  result /= voterc;
  return result;
}

double satisfaction_favorites(vector<vector<int>> const& opinions, vector<int> const& winners) {
  int const voterc = opinions.size(), winnerc = winners.size();

  double result = 0;
  for(auto &voter: opinions) {
    auto favorites = voter;
    sort(favorites.begin(), favorites.end(), greater<int>());
    favorites.resize(winnerc);
    for(int i = 0; i < winnerc; i++) {
      int x = voter[winners[i]] >= favorites.back() ? 1 : -1;
      if(are_winners_ordered) {
        x = x * (winnerc - i);
      }
      result += x;
    }
  }
  result *= opinion_range;
  if(are_winners_ordered) {
    result /= winnerc * (winnerc + 1) / 2;
  } else {
    result /= winnerc;
  }
  result /= voterc;
  return result;
}



array const satisfaction_methods = {
  satisfaction_linear,
  satisfaction_square,
  satisfaction_discrete,
  satisfaction_favorites
};
int const satisfactionc = satisfaction_methods.size();
array const satisfaction_names = {
  "linear",
  "square",
  "discrete",
  "favorites"
};
static_assert(satisfaction_names.size() == satisfactionc);

template<class AssignVotes>
vector<int> voting_boilerplate(int choicec, int winnerc, AssignVotes assign_votes) {
  assert(winnerc <= choicec);

  vector<int> votes(choicec, 0);
  assign_votes(votes);

  vector<int> winners(choicec);
  for(int i = 0; i < choicec; i++) {
    winners[i] = i;
  }
  sort(winners.begin(), winners.end(), [&](int a, int b) {
    return votes[a] > votes[b];
  });
  winners.resize(winnerc);
  return winners;
}

vector<int> voting_approval(vector<vector<int>> const& opinions, int winnerc) {
  int const choicec = opinions[0].size();
  return voting_boilerplate(choicec, winnerc, [&](vector<int> &votes) {
    for(auto &voter: opinions) {
      for(int i = 0; i < choicec; i++) {
        votes[i] += voter[i] > 0 ? 1 : 0;
      }
    }
  });
}

vector<int> voting_score(vector<vector<int>> const& opinions, int winnerc) {
  int const choicec = opinions[0].size();
  int const score_range = 10;
  return voting_boilerplate(choicec, winnerc, [&](vector<int> &votes) {
    for(auto &voter: opinions) {
      for(int i = 0; i < choicec; i++) {
        votes[i] += (voter[i] + opinion_range) * score_range / (2 * opinion_range + 1);
      }
    }
  });
}

vector<int> voting_first_past_the_post(vector<vector<int>> const& opinions, int winnerc) {
  int const choicec = opinions[0].size();
  return voting_boilerplate(choicec, winnerc, [&](vector<int> &votes) {
    for(auto &voter: opinions) {
      auto cmp = [&](int a, int b) {
        return voter[a] > voter[b];
      };
      priority_queue<int, vector<int>, decltype(cmp)> favorites(cmp);
      for(int i = 0; i < choicec; i++) {
        favorites.push(i);
        if(favorites.size() > winnerc) {
          favorites.pop();
        }
      }
      while(!favorites.empty()) {
        int choice = favorites.top();
        votes[choice]++;
        favorites.pop();
      }
    }
  });
}

vector<int> voting_ranked(vector<vector<int>> const& opinions, int winnerc) {
  int const choicec = opinions[0].size();
  return voting_boilerplate(choicec, winnerc, [&](vector<int> &votes) {
    for(auto &voter: opinions) {
      vector<int> ranked(choicec);
      for(int i = 0; i < choicec; i++) {
        ranked[i] = i;
      }
      sort(ranked.begin(), ranked.end(), [&](int a, int b) {
        return voter[a] < voter[b];
      });
      for(int i = 0; i < choicec; i++) {
        votes[ranked[i]] += i;
      }
    }
  });
}

vector<int> voting_random(vector<vector<int>> const& opinions, int winnerc) {
  int const choicec = opinions[0].size();
  assert(winnerc <= choicec);

  vector<int> winners;
  vector<bool> is_in(choicec, false);
  while(winners.size() < winnerc) {
    int candidate = uniform_int_distribution(0, choicec - 1)(rng);
    if(!is_in[candidate]) {
      winners.push_back(candidate);
      is_in[candidate] = true;
    }
  }
  return winners;
}

vector<int> voting_best(vector<vector<int>> const& opinions, int winnerc) {
  int const choicec = opinions[0].size();
  assert(winnerc <= choicec);

  vector<int> best_winners;
  double best_satisfaction;

  vector<int> winners;
  vector<bool> is_in(choicec, false);
  function<void()> backtrack = [&]() {
    if(winners.size() >= winnerc) {
      double satisfaction = satisfaction_methods[0](opinions, winners);
      if(best_satisfaction < satisfaction || best_winners.empty()) {
        best_satisfaction = satisfaction;
        best_winners = winners;
      }
      return;
    }
    for(int i = 0; i < choicec; i++) {
      if(is_in[i]) continue;
      winners.push_back(i);
      is_in[i] = true;
      backtrack();
      winners.pop_back();
      is_in[i] = false;
    }
  };
  backtrack();

  return best_winners;
}

array const voting_methods = {
  voting_approval,
  voting_score,
  voting_first_past_the_post,
  voting_ranked,
  voting_random,
  voting_best
};
int const votingc = voting_methods.size();
array const voting_names = {
  "approval",
  "score",
  "first-past-the-post",
  "ranked",
  "random",
  "best"
};
static_assert(voting_names.size() == votingc);



vector<vector<double>> simulate_vote(int voterc, int choicec, int winnerc, bool should_print = false) {
  vector opinions(voterc, vector<int>(choicec));
  for(auto &voter: opinions) {
    for(int &choice: voter) {
      choice = uniform_int_distribution(-opinion_range, opinion_range)(rng);
    }
  }

  if(should_print) {
    cout << "opinions:" << endl;
    for(int i = 0; i < voterc; i++) {
      cout << "  of voter " << i + 1 << ":";
      for(int choice: opinions[i]) {
        cout << " " << choice;
      }
      cout << endl;
    }

    cout << "results:" << endl;
  }

  vector results(votingc, vector<double>(satisfactionc));
  for(int i = 0; i < votingc; i++) {
    auto winners = voting_methods[i](opinions, winnerc);
    for(int j = 0; j < satisfactionc; j++) {
      results[i][j] = satisfaction_methods[j](opinions, winners);
    }
    if(should_print) {
      cout << "  for " << voting_names[i] << ":" << endl;
      cout << "    winners:";
      for(int winner: winners) {
        cout << " " << winner + 1;
      }
      cout << endl;
      cout << "    satisfaction:";
      for(int satisfaction: results[i]) {
        cout << " " << satisfaction;
      }
      cout << endl;
    }
  }

  return results;
}

void run(int voterc, int choicec, int winnerc) {
  int const simc = 1000000000 / voterc / choicec / winnerc;

  vector sum(votingc, vector<double>(satisfactionc, 0));
  for(int sim = 1; sim <= simc; sim++) {
    auto results = simulate_vote(voterc, choicec, winnerc);
    for(int i = 0; i < votingc; i++) {
      for(int j = 0; j < satisfactionc; j++) {
        sum[i][j] += results[i][j];
      }
    }
  }

  vector<int> order(votingc);
  for(int i = 0; i < votingc; i++) {
    order[i] = i;
  }
  sort(order.begin(), order.end(), [&](int a, int b) {
    return sum[a][0] > sum[b][0];
  });

  size_t name_pad = 0;
  for(auto &name: voting_names) {
    name_pad = max(name_pad, strlen(name));
  }

  cout << "voterc: " << voterc << "  choicec: " << choicec << "  winnerc: " << winnerc << "  simc: " << simc << endl;
  cout << fixed << setprecision(2);
  for(int i: order) {
    cout << voting_names[i] << ":";
    cout << setw(name_pad - strlen(voting_names[i]) + 1);
    for(int j = 0; j < satisfactionc; j++) {
      cout << " " << setw(5) << sum[i][j] / simc;
    }
    cout << endl;
  }
}

int main() {
  rng.seed(random_device()());

  cout << "satisfaction methods:";
  for(auto &name: satisfaction_names) {
    cout << " " << name;
  }
  cout << endl;

  cout << endl; run(20, 10, 3);
  cout << endl; run(20, 10, 1);
  cout << endl; run(20, 2, 1);
  cout << endl; run(1000, 10, 3);
  cout << endl; run(1000, 10, 1);
  cout << endl; run(1000, 2, 1);
}
