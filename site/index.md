---
layout: post
---
## Who am I?
<style>
#avatar {
  float: right;
  margin-left: 1em;
  margin-bottom: 1em;
  max-width: 33%;
  image-rendering: crisp-edges;
}
@media (max-width: calc(192px + 3em)) {
  #avatar {
    display: block;
    max-width: 100%;
  }
}
</style>
![My avatar](/static/avatar.png){:id="avatar" .rounded}

I'm a programmer going by "digitcrusher" on the internet, living in Poland and speaking almost fluently in English. I have been using Linux for everyday purposes since 2016. I'm also a free (as in freedom) software enjoyer and an active user of [Debian](https://www.debian.org/), [Kakoune](https://kakoune.org/), [Lzip](http://www.nongnu.org/lzip/) and [mpv](https://mpv.io/).

You can contact me via email: ![An image of my email address](/static/email.png){:style="vertical-align: sub; height: 1em;"}. Please use [my GnuPG public key](/static/1479909FF9E0FED0752C9E0906D4482EC50E0E41.asc) when sending messages to me if you can. [Here's a guide](https://emailselfdefense.fsf.org/) on how to create and use your own GnuPG key.

As you can probably guess, programming is my hobby and a big part of my life too. I started my coding adventure in 2014 and have been hooked ever since. Nowadays I write mainly competitive programming stuff in C++, and scripts and hobby projects in Python, Scala or any other language fit for the job.

After 7 years of switching between a variety of distros - Mint, Manjaro, Antergos, Alpine and Ubuntu - I have finally settled down in the simple, bloatless, modern land of Debian and wrote [my own preseed](https://gitlab.com/digitcrusher/lacian) for it that automatically tailors it to my personal needs.

## What is this website?
This is the place where I showcase my projects and write articles on things I find interesting. This whole website is just static HTML and CSS hosted on my Raspberry Pi using [lighttpd](https://www.lighttpd.net/). Having said my goodbyes to Jekyll's dependency hell and dodged the Hugo "Where's the docs?" bullet, I took one day out of my life to write [my own 190-LOC static site generator](https://gitlab.com/digitcrusher/my-website/-/blob/master/build.py) in Python, which may very well be the best and most flexible solution. Feel free to copy its design and structure for your own use.

# My works
I host my code mainly on [GitLab](https://gitlab.com/digitcrusher) and in rare cases on [GitHub](https://github.com/digitcrusher/). Here's a list of my projects I work on in my spare time:

- [Astrovoyageur](https://gitlab.com/digitcrusher/astrovoyageur) - A 2D physics demo in Scala and LWJGL (right now) and a finished moon lander simulator in the future. [Some](https://www.youtube.com/watch?v=GVIP4i4xbZU) [recordings](https://www.youtube.com/watch?v=arb-6cGJt0k) [of it.](https://www.youtube.com/watch?v=Q3OaW2wHAYU)
- [algorytmy](https://github.com/digitcrusher/algorytmy) - A collection of algorithms useful for competitive programming, written in C++ and documented in Polish.
- [bm](https://gitlab.com/digitcrusher/bm) - A sophisticated yet minimalist bookmarks manager written in Python.

…and the ones that I have finished or abandoned:

- [digitful](https://gitlab.com/digitcrusher/digitful) - A finished 2D OpenGL lighting demo in C using GLFW. [Here's a recording of it.](https://www.youtube.com/watch?v=o4RGK-XKzUg)
- [Discord voice channel observer bot](https://github.com/digitcrusher/Discord-voice-channel-observer-bot) - A Discord bot in Python that monitors activity in voice channels.
- [Country Outline Guessing Game](https://lacina.io/country-outline-guessing-game/) - A finished browser game where you have to guess a country by its shape. You can view the source code [here](https://gitlab.com/digitcrusher/country-outline-guessing-game).
- [billboard](https://gitlab.com/digitcrusher/billboard) - A simple two player eight-ball pool game in C++ and SFML, which I originally wrote in 24 hours at a camp for some friends. This one's abandoned.

These are open source of course. Some of them don't have any setup guides yet, so you only have the build scripts to work with.

# Posts
I may upload some interesting articles here from time to time. In the meantime, you can add this website to your [Atom syndication feed](/feed.xml) to be up-to-date with my upcoming undertakings.

{% for post in site.feed -%}
- <time datetime="{{ post.date | iso_date }}">{{ post.date | human_date }}</time> - <a href="{{ post.url }}">{{ post.title }}</a>
{% endfor %}
