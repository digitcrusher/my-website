#!/usr/bin/env python3

import os, re, requests, shutil, subprocess, sys, yaml
from bs4 import BeautifulSoup
from dataclasses import dataclass, field
from datetime import date, datetime
from htmlmin import Minifier
from jinja2 import Environment, FileSystemLoader
from markdown import markdown
from markupsafe import Markup
from time import time

should_minify = 'fast' not in map(lambda x: x.strip().casefold(), sys.argv[1:])

@dataclass
class Page:
  src: str
  dest: str
  modified: datetime
  content: str | BeautifulSoup
  excerpt: str | None

  layout: str | None
  title: str | None
  date: datetime | None
  shadow: bool

  def __init__(self, src):
    self.src = src

    if os.path.basename(self.src) == 'index.md':
      self.dest = os.path.join(os.path.dirname(self.src), 'index.html')
    elif os.path.splitext(self.src)[1] in {'.html', '.md'} and os.path.basename(self.src) != 'index.html':
      self.dest = os.path.join(os.path.splitext(self.src)[0], 'index.html')
    else:
      self.dest = self.src

    git_date = subprocess.run(
      ['git', 'log', '--pretty=%aI', '-1', os.path.join('site', self.src)],
      capture_output=True, check=True, text=True,
    ).stdout.strip()
    if git_date:
      self.modified = datetime.fromisoformat(git_date)
    else:
      self.modified = datetime.fromtimestamp(os.path.getmtime(os.path.join('site', self.src))).astimezone()

    with open(os.path.join('site', self.src)) as f:
      assert f.readline().strip() == '---'
      frontmatter = ''
      while (line := f.readline()).strip() != '---':
        frontmatter += line
      self.content = f.read()
      self.excerpt = None
    frontmatter = yaml.safe_load(frontmatter)

    self.dest = frontmatter.pop('dest', self.dest)
    self.layout = frontmatter.pop('layout', 'post' if self.is_post else None)
    self.title = frontmatter.pop('title', None)
    self.date = frontmatter.pop('date', None)
    if isinstance(self.date, date) and not isinstance(self.date, datetime):
      self.date = datetime(self.date.year, self.date.month, self.date.day).astimezone()
    self.shadow = frontmatter.pop('shadow', False)
    assert not frontmatter

  @property
  def url(self):
    if os.path.basename(self.dest) == 'index.html':
      if os.path.dirname(self.dest):
        return f'/{os.path.dirname(self.dest)}/'
      else:
        return '/'
    else:
      return f'/{self.dest}'

  @property
  def is_post(self):
    return self.src.startswith('posts/')

@dataclass
class Site:
  title: str
  description: str
  author: str
  url: str
  pages: list[Page] = field(default_factory=list)

  def abs_url(self, url):
    return f'{self.url.rstrip("/")}/{url.lstrip("/")}'

  @property
  def feed(self):
    return sorted(filter(lambda x: x.is_post and not x.shadow, self.pages), key=lambda x: x.date, reverse=True)

site = Site("Karol Łacina's Website", 'The place where I talk about things and make stuff', 'Karol Łacina', 'https://lacina.io/')

start_time = time()

try:
  shutil.rmtree('build/')
except FileNotFoundError:
  pass
os.mkdir('build/')

for enclosing, _, files in os.walk('site/'):
  for file in files:
    file = os.path.join(enclosing, file).removeprefix('site/')
    ext = os.path.splitext(file)[1]

    src = os.path.join('site', file)
    dest = os.path.join('build', file)

    if ext in {'.html', '.md', '.xml'}:
      print(f'Parsing {file!r}')
      site.pages.append(Page(file))

    elif ext == '.css' and should_minify:
      print(f'Minifying {file!r}')
      with open(src) as f:
        response = requests.post('https://www.toptal.com/developers/cssminifier/api/raw', data={
          'input': f.read(),
        })
      response.raise_for_status()
      os.makedirs(os.path.dirname(dest), exist_ok=True)
      with open(dest, 'x') as f:
        f.write(response.text)

    else:
      print(f'Copying {file!r}')
      os.makedirs(os.path.dirname(dest), exist_ok=True)
      shutil.copyfile(src, dest)

a, b = [], []
for page in site.pages:
  if os.path.basename(page.dest) == 'feed.xml':
    b.append(page)
  else:
    a.append(page)
site.pages = a + b

def anchorate(soup):
  if not isinstance(soup, BeautifulSoup):
    soup = BeautifulSoup(soup, 'lxml')

  for heading in soup.find_all(re.compile(r'h[1-6]')):
    if 'id' not in heading:
      slug = ''
      for c in heading.text:
        if c.isalnum():
          slug += c.casefold()
        elif c not in "'’" and slug and not slug.endswith('-'):
          slug += '-'
      slug = slug.removesuffix('-')

      heading['id'] = slug
      anchor = soup.new_tag('a', href=f'#{slug}')
      anchor['class'] = 'anchor'
      anchor.string = '🔗'
      heading.append(anchor)

  return soup

env = Environment(loader=FileSystemLoader('layouts/'), autoescape=True)
env.filters |= {
  'abs_url': site.abs_url,
  'anchorate': anchorate,
  'human_date': lambda x: (datetime.fromisoformat(x) if isinstance(x, str) else x).strftime('%d %b %Y'),
  'iso_date': lambda x: x.isoformat(),
  'smartify': lambda x: Markup(markdown(x, extensions=['smarty']).removeprefix('<p>').removesuffix('</p>')), # Hacky but works
}

minifier = Minifier(remove_comments=True, remove_empty_space=True)

for page in site.pages:
  print(f'Rendering {page.src!r}')

  page.content = env.from_string(page.content).render(site=site, page=page)
  if page.src.endswith('.md'):
    page.content = markdown(page.content, extensions=['attr_list', 'codehilite', 'fenced_code', 'smarty', 'tables'])
  if page.dest.endswith('.html'):
    page.content = BeautifulSoup(page.content, 'lxml')
    page.excerpt = page.content.find(lambda x: x.name == 'p' and x.text).text
  if page.layout is not None:
    page.content = env.get_template(f'{page.layout}.html').render(site=site, page=page)
  if page.dest.endswith('.html') and should_minify:
    page.content = minifier.minify(page.content)

  dest = os.path.join('build', page.dest)
  os.makedirs(os.path.dirname(dest), exist_ok=True)
  with open(dest, 'x') as f:
    f.write(page.content)

print(('Production' if should_minify else 'Development') + f' build done in {time() - start_time:.2f}s')
